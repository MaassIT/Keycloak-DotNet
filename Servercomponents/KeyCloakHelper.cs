using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.IdentityModel.Tokens;

// ReSharper disable ClassNeverInstantiated.Local
// ReSharper disable CollectionNeverUpdated.Local
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Servercomponents
{
    public static class KeyCloakHelper
    {
        private class KeysCollection
        {
            public List<JsonWebKey> Keys { get; set; } = new();
        }

        public static async Task<List<SecurityKey>> AddKeyCloakAsync(string host,
            string tenant)
        {
            using var client = new HttpClient();
            // var issuer = Issuer.FromJson(await client.GetStringAsync($"https://{host}/auth/realms/{tenant}"));


            var jwt = await client.GetStringAsync($"https://{host}/auth/realms/{tenant}/protocol/openid-connect/certs");

            // System.Diagnostics.Debug.WriteLine($"JWT erfolgreich geladen: {jwt}");

            // var x509Key = new X509SecurityKey(new X509Certificate2(Convert.FromBase64String(issuer.PublicKey)));
            // rsaKey.Rsa.ImportRSAPublicKey(Encoding.UTF8.GetBytes(issuer.PublicKey), out var i);

            var data = JsonSerializer.Deserialize<KeysCollection>(jwt, new JsonSerializerOptions()
            {
                PropertyNameCaseInsensitive = true
            });

            var keys = GetSecurityKeys(data.Keys);

            // builder.AddJwtBearer(options =>
            // {
            //     options.IncludeErrorDetails = true;
            //     options.RequireHttpsMetadata = false;
            //     
            //     options.TokenValidationParameters = new TokenValidationParameters
            //     {
            //         ValidateLifetime = true,
            //         ValidateIssuerSigningKey = true,
            //         ValidateAudience = false,
            //         ValidateIssuer = false,
            //         IssuerSigningKeys = keys
            //     };
            // });
            // Console.WriteLine($"Auth Zertifikat hinzugefügt: {keys[0].KeyId.Substring(0,10)}...");
            return keys;
        }

        private static byte[] FromBase64Url(string base64Url)
        {
            string padded = base64Url.Length % 4 == 0
                ? base64Url
                : base64Url + "====".Substring(base64Url.Length % 4);
            string base64 = padded.Replace("_", "/")
                .Replace("-", "+");
            return Convert.FromBase64String(base64);
        }

        [SuppressMessage("ReSharper", "RedundantDefaultMemberInitializer")]
        public partial class Issuer
        {
            [JsonPropertyName("realm")] public string Realm { get; set; } = null!;

            [JsonPropertyName("public_key")] public string PublicKey { get; set; } = null!;

            [JsonPropertyName("token-service")] public Uri TokenService { get; set; } = null!;

            [JsonPropertyName("account-service")] public Uri AccountService { get; set; } = null!;

            [JsonPropertyName("tokens-not-before")]
            public long TokensNotBefore { get; set; }
        }

        public partial class Issuer
        {
            public static Issuer? FromJson(string json) =>
                JsonSerializer.Deserialize<Issuer>(json);
        }

        private static class Converter
        {
            // public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
            // {
            //     MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            //     DateParseHandling = DateParseHandling.None,
            //     Converters =
            //     {
            //         new IsoDateTimeConverter {DateTimeStyles = DateTimeStyles.AssumeUniversal}
            //     },
            // };
        }

        private static List<SecurityKey> GetSecurityKeys(IEnumerable<JsonWebKey> jsonWebKeySet)
        {
            var keys = new List<SecurityKey>();

            foreach (var key in jsonWebKeySet)
            {
                if (key.Kty == "RSA")
                {
                    if (key.X5c != null && key.X5c.Any())
                    {
                        string certificateString = key.X5c[0];
                        var certificate = new X509Certificate2(Convert.FromBase64String(certificateString));

                        var x509SecurityKey = new X509SecurityKey(certificate)
                        {
                            KeyId = key.Kid
                        };

                        keys.Add(x509SecurityKey);
                    }
                    else if (!string.IsNullOrWhiteSpace(key.E) && !string.IsNullOrWhiteSpace(key.N))
                    {
                        byte[] exponent = FromBase64Url(key.E);
                        byte[] modulus = FromBase64Url(key.N);

                        var rsaParameters = new RSAParameters
                        {
                            Exponent = exponent,
                            Modulus = modulus
                        };

                        var rsaSecurityKey = new RsaSecurityKey(rsaParameters)
                        {
                            KeyId = key.Kid
                        };

                        keys.Add(rsaSecurityKey);
                    }
                    else
                    {
                        throw new Exception("JWK data is missing in token validation");
                    }
                }
                else
                {
                    throw new NotImplementedException("Only RSA key type is implemented for token validation, key is " +
                                                      key.Kty);
                }
            }

            return keys;
        }
    }
}