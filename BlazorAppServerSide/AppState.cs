﻿using Blazorcomponents;
using Blazorcomponents.Enums;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;

namespace BlazorAppServerSide
{
    public class AppState
    {
        public KeyCloak KeyCloak { get; }

        public AppState(IJSRuntime jsRuntime, IConfiguration configuration)
        {
            KeyCloak = new KeyCloak(jsRuntime, configuration["KeyCloak:Realm"], configuration["KeyCloak:AuthServerUrl"], configuration["KeyCloak:ClientId"])
            {
                KeycloakSettings =
                {
                    OnLoadAction = OnLoadAction.LoginRequired,
                    EnableLogging = true,
                }
            };
        }
    }
}
