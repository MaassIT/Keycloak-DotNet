﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

namespace BlazorAppServerSide
{
    public partial class App
    {
        [Inject] private IJSRuntime JsRuntime { get; set; } = null!;
        [Inject] private HttpClient HttpClient { get; set; } = null!;

        protected override void OnInitialized()
        {
            JsRuntime.InvokeVoidAsync("DotNetHelper", DotNetObjectReference.Create(this));

            AppState.KeyCloak.UserLoginStateChangedEvent += (_, _) =>
            {
                HttpClient.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("bearer", AppState.KeyCloak.LoginState.Token);

                Console.WriteLine(AppState.KeyCloak.LoginState.Token);

                StateHasChanged();
            };

            base.OnInitialized();
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                await AppState.KeyCloak.InitAsync();
            }
            await base.OnAfterRenderAsync(firstRender);
        }
    }
}
