using System;
using System.Net.Http;
using BlazorAppServerSide.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Servercomponents;

namespace BlazorAppServerSide
{
    public class Startup
    {
        private const string CorsPolicyName = "Allow Release Environment";
        private const string DevCorsPolicyName = "Allow Anything";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // ### EXAMPLE BEGIN ###

            var host = Configuration["KeyCloak:Host"];
            var realm = Configuration["KeyCloak:Realm"];
            var authenticationBuilder = services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme);
            var keys = KeyCloakHelper.AddKeyCloakAsync(host, realm).Result;

            authenticationBuilder.AddJwtBearer(options =>
            {
                options.IncludeErrorDetails = true;
                options.RequireHttpsMetadata = false;

                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidateAudience = false,
                    ValidateIssuer = false,
                    IssuerSigningKeys = keys
                };
            });

            services.AddCors(options =>
            {
                options.AddPolicy(name: CorsPolicyName,
                    builder =>
                    {
                        builder
                            .SetIsOriginAllowedToAllowWildcardSubdomains()
                            .WithOrigins("https://localhost",
                                "https://*.snackbee.de")
                            .AllowAnyHeader().AllowAnyMethod();
                    });
                options.AddPolicy(name: DevCorsPolicyName,
                    builder => { builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod(); });
            });

            // ### EXAMPLE END ###

            services.AddRazorPages();
            services.AddServerSideBlazor();

            // ### EXAMPLE BEGIN ###

            services.AddScoped<HttpClient>();
            services.AddScoped<AppState>();

            // ### EXAMPLE END ###

            services.AddSingleton<WeatherForecastService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            // ### EXAMPLE BEGIN ###
            // Must be done after app.UseRouting(); !!!

            app.UseCors(env.IsDevelopment() ? DevCorsPolicyName : CorsPolicyName);
            //app.UseAuthentication();
            //app.UseAuthorization();

            // ### EXAMPLE END ###

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
