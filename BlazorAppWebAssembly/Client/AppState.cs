﻿using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using BlazorAppWebAssembly.Shared;
using Blazorcomponents;
using Blazorcomponents.Enums;
using Microsoft.JSInterop;

namespace BlazorAppWebAssembly.Client
{
    public class AppState
    {
        public KeyCloak KeyCloak { get; }
        private readonly HttpClient _httpClient;

        public AppState(IJSRuntime jsRuntime, HttpClient httpClient)
        {
            const string keyCloakRealm = "realmname";
            _httpClient = httpClient;
            const string keyCloakAuthServerUrl = "https://cloud-login.pro/auth";
            const string keyCloakClientId = "clientid";
            KeyCloak = new KeyCloak(jsRuntime, keyCloakRealm, keyCloakAuthServerUrl, keyCloakClientId)
            {
                KeycloakSettings =
                {
                    OnLoadAction = OnLoadAction.LoginRequired,
                    EnableLogging = true,
                }
            };
        }

        public async Task<User?> GetUserAsync()
        {
            var userJson = await _httpClient.GetStringAsync("/User/Me");

            if (string.IsNullOrEmpty(userJson))
            {
                return null;
            }

            return JsonSerializer.Deserialize<User>(userJson, new JsonSerializerOptions()
            {
                PropertyNameCaseInsensitive = true
            });
        }
    }
}
