﻿namespace BlazorAppWebAssembly.Shared
{
    public class User
    {
        public string Mail { get; set; }

        public User(string mail)
        {
            Mail = mail;
        }
    }
}
