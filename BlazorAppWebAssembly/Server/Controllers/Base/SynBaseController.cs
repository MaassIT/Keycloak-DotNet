﻿using Microsoft.AspNetCore.Mvc;

namespace BlazorAppWebAssembly.Server.Controllers.Base
{
    [SynAuthorize]
    public class SynBaseController : ControllerBase
    {
        protected string Email => HttpContext!.Items["Email"] as string;
    }
}
