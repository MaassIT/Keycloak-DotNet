﻿using System.Linq;
using BlazorAppWebAssembly.Server.Controllers.Base;
using BlazorAppWebAssembly.Shared;
using Microsoft.AspNetCore.Mvc;

namespace BlazorAppWebAssembly.Server.Controllers
{
    public class UserController : SynBaseController
    {
        [HttpGet]
        [Route("Me")]
        public User? Get() => UserRepo.Users.SingleOrDefault(u => u.Mail == Email);
    }
}
