﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace BlazorAppWebAssembly.Server
{
    public class SynAuthorizeAttribute : TypeFilterAttribute
    {
        public SynAuthorizeAttribute() : base(typeof(SynAuthorizeFilter))
        {
        }
    }

    public class SynAuthorizeFilter : IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var email = context.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress")?.Value;
            if (email == null)
            {
                context.Result = new UnauthorizedResult();
            }
            else
            {
                context.HttpContext.Items.Add(new KeyValuePair<object, object>("Email", email));
            }
        }
    }
}
