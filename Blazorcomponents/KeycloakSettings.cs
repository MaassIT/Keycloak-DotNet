using System;
using Blazorcomponents.Enums;
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Blazorcomponents
{
    public class KeycloakSettings
    {
        /// <summary>
        /// Standardfall, wenn man nicht über die Variablen geht:
        /// /keycloak.json
        /// </summary>
        public string? KeycloakConfigFile { get; set; }

        public string? Realm { get; set; }
        public string? AuthServerUrl { get; set; }
        public string? ClientId { get; set; }
        public ClientFlow ClientFlow { get; set; } = ClientFlow.Default;
        public bool UseIFrame { get; set; } = true;
        public string? IdpHint { get; set; }
        public bool EnableLogging { get; set; }
        public Flow Flow { get; set; } = Flow.Standard;
        public OnLoadAction OnLoadAction { get; set; } = OnLoadAction.None;
        public string OnLoadString => OnLoadAction switch
        {
            OnLoadAction.None => "",
            OnLoadAction.CheckSso => "check-sso",
            OnLoadAction.LoginRequired => "login-required",
            _ => throw new ArgumentOutOfRangeException()
        };
        public string FlowAsString => Flow.ToString().ToLower();
    }
}