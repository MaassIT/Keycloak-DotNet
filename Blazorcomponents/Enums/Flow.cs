namespace Blazorcomponents.Enums
{
    public enum Flow
    {
        Standard,
        Implicit,
        Hybrid,
    }
}