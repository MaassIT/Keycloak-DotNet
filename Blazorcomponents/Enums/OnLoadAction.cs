namespace Blazorcomponents.Enums
{
    public enum OnLoadAction
    {
        /// <summary>
        /// No default authentication, only if requested
        /// </summary>
        None = 0,

        /// <summary>
        /// CheckSso will only authenticate the client if the user is already logged-in, if the user is not logged-in
        /// the browser will be redirected back to the application and remain unauthenticated.
        /// </summary>
        CheckSso = 1,

        /// <summary>
        /// LoginRequired will authenticate the client if the user is logged-in to Keycloak or display the login page if
        /// not.
        /// </summary>
        LoginRequired = 2,
    }
}