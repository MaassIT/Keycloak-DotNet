// noinspection JSUnresolvedVariable,JSUnresolvedFunction

let keycloak;
let BlazorApp;

export function SetBlazorApp(dotNetObject) {
    BlazorApp = dotNetObject;
}

function isSafari() {
    const is_chrome = navigator.userAgent.indexOf('Chrome') > -1;
    let is_safari = navigator.userAgent.indexOf("Safari") > -1;
    if ((is_chrome) && (is_safari)) {
        is_safari = false;
    }
    return is_safari;
}

let use_LoginIframe = !isSafari();

function ConfigKeyCloak(settings) {
    keycloak = new Keycloak({
        url: settings.authServerUrl,
        realm: settings.realm,
        clientId: settings.clientId,
    });

    keycloak.onAuthSuccess = function () {
        console.log(keycloak.tokenParsed["email"] + " Authentifiziert");
        BlazorApp.invokeMethodAsync('RegisterToken', String(keycloak.token));
    };

    keycloak.onAuthRefreshSuccess = function () {
        BlazorApp.invokeMethodAsync('RegisterToken', String(keycloak.token));
    };

    keycloak.onAuthRefreshError = function () {
        keycloak.login({
            idpHint: window.keycloakSettings.idpHint
        });
    };

    keycloak.onTokenExpired = function () {
        keycloak.updateToken();
    };

    keycloak.onAuthLogout = function () {
        console.log('Logged out');
    };
}

export function Login(settings) {
    keycloak.login({
        idpHint: settings.idpHint
    })
}

export function KeyCloakInit(settings) {
    window.keycloakSettings = settings;
    console.log(settings);

    ConfigKeyCloak(settings);

    keycloak
        .init({
            promiseType: 'native',
            enableLogging: settings.enableLogging,
            checkLoginIframe: use_LoginIframe,
            onLoad: settings.onLoadString || null,
            idpHint: settings.idpHint,
            flow: settings.flowAsString,
            silentCheckSsoRedirectUri: window.location.origin + '/_content/Blazorcomponents/silent-check-sso.html'
        })
        .then(function () {
            // Check the Token every 15 seconds
            setInterval(function () {
                keycloak.updateToken(15);
            }, 15000);
        })
        .catch(function () {
            alert('Error on User Authentification!');
        });

}

export function logout() {
    console.log("Logout User.");
    keycloak.logout();
}
