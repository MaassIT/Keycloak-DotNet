using System;
using Microsoft.AspNetCore.Components;

namespace Blazorcomponents
{
    public class LoginState
    {
        public bool IsLoggedIn { get; set; }
        public string Token { get; set; }
    }
}