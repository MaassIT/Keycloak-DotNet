using System;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace Blazorcomponents
{
    public class KeyCloak
    {
        private readonly IJSRuntime _jsRuntime;
        private IJSObjectReference? _module;
        private IJSObjectReference? _keycloak;
        public KeycloakSettings KeycloakSettings { get; set; } = new();

        public event EventHandler UserLoginStateChangedEvent = null!;
        public Func<Task>? OnAfterSuccessfullLoggedIn { get; set; }
        public readonly LoginState LoginState = new();

        public KeyCloak(IJSRuntime jsRuntime, string keycloakConfigFile = "/keycloak.json")
        {
            _jsRuntime = jsRuntime;
            KeycloakSettings.KeycloakConfigFile = keycloakConfigFile;
        }

        public KeyCloak(IJSRuntime jsRuntime, string? realm, string? authServerUrl, string? clientId)
        {
            KeycloakSettings.Realm = realm;
            KeycloakSettings.AuthServerUrl = authServerUrl;
            KeycloakSettings.ClientId = clientId;
            _jsRuntime = jsRuntime;
        }

        public async Task<KeyCloak> InitAsync()
        {
            _keycloak = await _jsRuntime.InvokeAsync<IJSObjectReference>(
                "import", KeycloakSettings.AuthServerUrl + "/js/keycloak.js");
            _module = await _jsRuntime.InvokeAsync<IJSObjectReference>(
                "import", "./_content/Blazorcomponents/loginService.js");

            await _module.InvokeVoidAsync("SetBlazorApp", DotNetObjectReference.Create(this));
            await _module.InvokeVoidAsync("KeyCloakInit", KeycloakSettings);

            return this;
        }

        public async Task LogoutAsync()
        {
            Console.WriteLine("################# Super toller LOGOUT!!!");
            if (_module == null) return;

            await _module.InvokeVoidAsync("logout");
            UserLoginStateChangedEvent.Invoke(this, EventArgs.Empty);
        }

        [JSInvokable("RegisterToken")]
        public async Task RegisterToken(string token)
        {
            LoginState.Token = token;
            LoginState.IsLoggedIn = true;
            if (OnAfterSuccessfullLoggedIn != null) 
                await OnAfterSuccessfullLoggedIn();
            UserLoginStateChangedEvent.Invoke(this, EventArgs.Empty);
        }
    }
}